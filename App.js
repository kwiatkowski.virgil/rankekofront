
import {Inscription} from './Pages/Inscription/Inscription';
import { AppInformations } from './Pages/AppInformations/AppInformations';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RegistrationDriver } from './Pages/RegistrationDriver/RegistrationDriver';
import { StartCourse } from './Pages/StartCourse/StartCourse';
import { Course } from './Pages/Course/Course';


export default function App() {
  const Stack = createNativeStackNavigator();
  // changement du background par défaut du navigator
  const navTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: 'transparent',
    },
  };
  return (
    <NavigationContainer theme={navTheme}>
      <Stack.Navigator 
      initialRouteName="Course" 
      screenOptions={{
        headerShown: false,
      }}>
        <Stack.Screen name="Inscription" component={Inscription} />
        <Stack.Screen name="AppInformations" component={AppInformations} />
        <Stack.Screen name="RegistrationDriver" component={RegistrationDriver} />
        <Stack.Screen name="StartCourse" component={StartCourse} />
        <Stack.Screen name="Course" component={Course} options={{
        animationEnabled: false,
      }}/>
      </Stack.Navigator>
    </NavigationContainer>
);
}