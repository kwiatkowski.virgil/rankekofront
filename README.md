# RankekoFront

La dernière version est sur la branche develop.

pour faire fonctionner le projet, besoin de lancer l'api, il s'agit du projet suivant : 

https://gitlab.com/kwiatkowski.virgil/rankekoback qui fonctionne sous symfony avec php 8.1 et API platform.

Lors de la création d'un compte, ce dernier doit etre accepté par le backoffice. Passer le boolean "account_verified" à true en base de données pour le vérifier et accéder au reste de l'application. 

OU : 

installez le backoffice : 

https://gitlab.com/kwiatkowski.virgil/rankekobo


En cas de bug, décommentez ligne 64 à 66 dans App.js. puis commentez de nouveau. 

Pas d'APK pour le moment, car le projet n'est pour le moment pas fonctionnel sur android. 


Plus d'informations, ou Apk (pour le moment non fonctionnel sur android) : kwiatkowski.virgil@gmail.com


