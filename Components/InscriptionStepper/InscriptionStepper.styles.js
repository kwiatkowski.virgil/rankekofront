import { StyleSheet } from 'react-native';
import {colors} from '../../assets/colors/colors';
export const styles = StyleSheet.create({
    iconContainer:{
        display : 'flex',
        justifyContent : 'space-between',
        flexDirection : 'row',
        width : '100%',
        position: 'absolute',
        top : '-100%',
    
    },
    iconContainerIcon:{
        color : 'white',
        margin : 10,
    },
    iconContainerDiv:{
        color : 'white',
        margin : 10,
        borderRadius : 50,
        backgroundColor : colors.blueSecondary,
    },
    iconContainerDivActive:{
        color : 'white',
        margin : 10,
        borderRadius : 50,
        backgroundColor : colors.greenPrimary,
    },
    iconBackground:{
        marginTop : 100,
        display:'flex',
        height: 20,
        width: '100%' ,
        backgroundColor: '#fff',
        borderRadius: 50,
        position : 'relative',
        shadowColor: '#2C3E50',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        shadowRadius: 4,
    },
})