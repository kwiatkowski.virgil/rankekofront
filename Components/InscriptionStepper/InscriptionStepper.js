import {View } from 'react-native';
import { styles } from './InscriptionStepper.styles';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faAt, faUserCircle, faLock, faCar } from '@fortawesome/free-solid-svg-icons';

export const  InscriptionStepper = ({active}) => {
    const steps = [
        {id : 1, icon : faAt},
        {id : 2, icon : faUserCircle},
        {id : 3, icon : faLock},
        {id : 4, icon : faCar}
    ]
  return (
    <View style={styles.iconBackground}>
        <View style={styles.iconContainer}>
            {steps.map(({icon,id})=>(
                <>
                {active=== id  ? (
                    <View key={id} style={ styles.iconContainerDivActive }><FontAwesomeIcon style={styles.iconContainerIcon} icon={ icon } /></View>
                ):(
                    <View key={id} style={ styles.iconContainerDiv }><FontAwesomeIcon style={styles.iconContainerIcon} icon={ icon } /></View>
                )}
                </>
           ) )}
        </View>
    </View>  
);
}

