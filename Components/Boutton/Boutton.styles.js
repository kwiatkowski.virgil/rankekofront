import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  viewBoutton: {
    display : 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'relative',
    bottom : 0
  },
  boutton: {
    backgroundColor: '#EF7C60',
    textAlign: 'center',
    borderRadius: 29,
    width: 348,
    height: 58,
    top:370

  },
  bouttonText: {
    color: '#fff',
    padding: 15,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign : 'center',
  }
});