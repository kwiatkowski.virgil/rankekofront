import * as React from 'react';
import { View, Text, Pressable } from 'react-native';

import { styles } from './Boutton.styles';

export function Boutton({functionPerso, label,color}) {
 return (
  
  <View style={styles.viewBoutton}>
   <Pressable 
        onPress={functionPerso} 
        style= {{
            backgroundColor: color,
            textAlign: 'center',
            borderRadius: 29,
            width: 348,
            height: 58
            }}>
    <Text style={styles.bouttonText}>{label}</Text>
   </Pressable>
  </View>
 );
}




