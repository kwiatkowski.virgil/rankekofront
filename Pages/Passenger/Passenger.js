import React, {useState} from 'react';

import { styles } from './Passenger.styles'
import { SlidePassenger } from './SlidePassenger/SlidePassenger'
import { Boutton } from '../../Components/Boutton/Boutton'
import imageSlide1 from '../../assets/images/onboarding.png'
import imageSlide2 from '../../assets/images/Passager-etape3.png'
import imageSlide3 from '../../assets/images/OnboardingConducteurEtape2.png'
import imageSlide4 from '../../assets/images/ConducteurEtape3.png'

export function Passenger() {

  const [step, setStep] = useState(1)
  console.log(step);
  return (
    <>
    {step===1 &&
     <>
      <SlidePassenger image={imageSlide1} textInfo={'En passager, commencez par cliquer sur le bouton "Rejoindre un trajet"'} circle={styles.circle }/> 
      <Boutton functionPerso={() => setStep(2)} label={"Continuer"} />
    </>}
    {step===2 && 
      <>
        <SlidePassenger image={imageSlide2} textInfo={'RDV au point du rendez-vous et démarrer le trajet'} circle={styles.circle}/>
        <Boutton functionPerso={() => setStep(3)} label={"Continuer"} />
    </>}
    {step===3 && 
      <>
        <SlidePassenger image={imageSlide3} textInfo={'Scanner le QR code du conducteur'} circle={styles.circle}/>
        <Boutton functionPerso={() => setStep(4)} label={"Continuer"} />
    </>}
    {step===4 && 
      <>
        <SlidePassenger image={imageSlide4} textInfo={'Récuperez vos points en cliquant sur'} circle={styles.circle}/>
        <Boutton functionPerso={() => console.log('coucou')} label={"Ok, j'ai compris"} />
    </>}
    
    </>
);

}
