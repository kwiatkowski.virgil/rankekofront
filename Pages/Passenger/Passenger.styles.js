import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  content: {
    color:'white',
    display : 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  divImage: {
    display : 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'relative',
    width: '100%',
    height: '100%'
   },
    tinyLogo: { 
    padding: 10,
    top: 50,
  },
  background: {
    width:'100%',
    position: 'absolute'
  },
  textInfo: {
    fontSize: 30,
    lineHeight : 50,
    color: '#2C3E50',
    fontWeight: 'bold',
    padding: 30,
    display : 'flex',
    justifyContent: 'center',
  }, 
  divContent : {
      marginTop: 100
  },
  circleContainer: {
    flexWrap: 'wrap', 
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
    marginBottom : 50
  }
});