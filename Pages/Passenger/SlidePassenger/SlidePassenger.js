import * as React from 'react';
import { Text, View, Image } from 'react-native';
import { styles } from './SlidePassenger.styles';
import imageBackground from '../../../assets/vague.png';
// import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
// import { faArrowLeft } from '@fortawesome/react-native-fontawesome';

export function SlidePassenger({image, textInfo, circle}) {

  return (
    <View style={styles.content}>
      {/* <View>
        <FontAwesomeIcon icon={ faArrowLeft } />
      </View> */}
      <View style={styles.divImage}>
        <Image style={styles.background} source={imageBackground} />
        <Image style={styles.tinyLogo} source={image} />
      </View>
      <View style={styles.divContent}>
        <Text style={styles.textInfo}>{textInfo}</Text>
      </View>
      <View style={styles.circleContainer}>
        <Text style={circle}></Text>
        <Text style={circle}></Text>
        <Text style={circle}></Text>
      </View>
    </View>
 );

}