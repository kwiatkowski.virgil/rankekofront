import React, {useState} from 'react';

import { styles } from './Driver.styles'
import { Slide } from './Slide/Slide'
import { Boutton } from '../../Components/Boutton/Boutton'
import imageSlide1 from '../../assets/images/onboarding.png'
import imageSlide2 from '../../assets/images/OnboardingConducteurEtape2.png'
import imageSlide3 from '../../assets/images/ConducteurEtape3.png'
import imageSlide4 from '../../assets/images/ConducteurEtape3.png'

export function Driver() {

  const [step, setStep] = useState(1)
  console.log(step);
  return (
    <>
    {step===1 &&
     <>
      <Slide image={imageSlide1} textInfo={'En conducteur, générez un QR code à l\'aide du bouton "Démarrer un trajet"'} circle={styles.circle }/> 
      <Boutton functionPerso={() => setStep(2)} label={"Continuer"} />
    </>}
    {step===2 && 
      <>
        <Slide image={imageSlide2} textInfo={'Demandez aux passagers de scanner le QR code'} circle={styles.circle}/>
        <Boutton functionPerso={() => setStep(3)} label={"Continuer"} />
    </>}
    {step===3 && 
      <>
        <Slide image={imageSlide3} textInfo={'Appuyez sur le bouton Lancer le trajet'} circle={styles.circle}/>
        <Boutton functionPerso={() => setStep(4)} label={"Continuer"} />
    </>}
    {step===4 && 
      <>
        <Slide image={imageSlide4} textInfo={'Récuperez vos points en cliquant sur'} circle={styles.circle}/>
        <Boutton functionPerso={() => console.log('coucou')} label={"Ok, j'ai compris"} />
    </>}
    
    </>
 );

}
