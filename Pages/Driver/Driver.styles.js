import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
 
  background: {
    width:'100%',
    height: '100%',
    position: 'absolute'
  },
  textInfo: {
    fontSize: 30,
    color: '#2c3e50',
    fontWeight: 'bold',
    padding: 20,
    display : 'flex',
    justifyContent: 'center',
  },
  circleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent:'center',
    padding: 100,
  },
  circle: {
    width: 16,
    height: 16,
    borderRadius: 20,
    backgroundColor: '#2c3e50',
    display : 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'relative',
    margin: 10,
   }
});