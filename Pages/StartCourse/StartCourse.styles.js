import { StyleSheet } from "react-native";
import { colors } from "../../assets/colors/colors";
export const styles = StyleSheet.create({
// HEADER
header : {
    backgroundColor : colors.blueSecondary,
    width : '100%',
    paddingTop : 60,
    paddingBottom : 40,
    position : 'relative',
    shadowColor: colors.blueSecondary,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
},
headerTitle : {
    color : "#fff",
    fontSize : 20,
    textAlign : 'center',
    fontWeight : 'bold'
},
headerArrowLeft : {
    color : colors.greenPrimary,
    width : 10,
    textAlign : 'left',
    position : 'absolute',
    left : 30
},
// MAIN
mainTitle :{
    color : colors.blueSecondary,
    width : "80%",
    fontSize : 30,
    fontWeight : 'bold',
    textAlign : "center",
    marginHorizontal: "10%",
    marginTop : "20%",
    marginBottom : "20%",
},
mainImage : {
    marginHorizontal: "5%",
    marginBottom : "20%"
},
mainQRcode : {
    position : "absolute",
    top : "50%",
    left: "40%",
}

});
