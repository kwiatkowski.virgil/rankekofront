import React, {useState, useEffect} from "react";
import { View,Text, TouchableOpacity,Image,StyleSheet } from "react-native";
import { styles } from './StartCourse.styles';
import { Boutton } from "../../Components/Boutton/Boutton";
import { faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import backgroundQRcode from "../../assets/images/backgroundQRcode.png";
import { colors } from "../../assets/colors/colors";
import QRCode from 'react-native-qrcode-svg';
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import { BarCodeScanner } from 'expo-barcode-scanner';

export const StartCourse = ({ navigation}) => {
const QRcodevalue = uuid();
const [hasPermission, setHasPermission] = useState(null);
const [scanned, setScanned] = useState(false);
useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
   };
  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

return(

<View style={styles.container}>
    <View style={styles.header}>
        <TouchableOpacity onPress={()=>setStep(2)}><FontAwesomeIcon style={styles.headerArrowLeft} icon={faArrowLeft} size={25} /></TouchableOpacity>
        <Text style={styles.headerTitle}>Démarrer un trajet </Text>
    </View>
    <View>
        <Text style={styles.mainTitle}>Montrer le QR code aux passagers</Text>
        <Image style={styles.mainImage} source={backgroundQRcode}/>
        <Boutton style={styles.mainButton} color={colors.orangeTertiary} label={"continuer"}/>
        <View style={styles.mainQRcode}><QRCode value={QRcodevalue}  /></View>
    </View>
    <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {scanned && navigation.navigate('Course')}
</View>
)};