import { StyleSheet } from 'react-native';
import {colors} from '../../assets/colors/colors';

export const styles = StyleSheet.create({
  imageBackground :{
    position : 'absolute',
    bottom : -200,
    right : 50,
  },
  inscriptionContainer :{
    paddingTop : 50,
    width : '80%',
    marginHorizontal : '10%',
    position : 'relative',
    minHeight:'100%',
  },
  title : {
      marginTop: 100,
      fontWeight: 'bold',
      fontSize : 30,
      opacity: .7
  },
  input : {
      marginTop : 10,
      marginBottom : 30,
      height : 40,
      width : '100%',
      textAlign: 'center',
      backgroundColor: "#fff",
      shadowColor: colors.blueSecondary,
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.2,
      shadowRadius: 4,
      borderRadius : 10,
  },
  inputTitle : {
      marginTop : 20,
  },
  text :{
      marginTop : 30,
      lineHeight : 25,
      fontSize : 20,
      lineHeight : 30,
  },
  button : {
      position:'absolute',
      bottom : 100,
  },
  imageBackground : {
      position : 'absolute',
      left : -150,
      bottom : -100,
      zIndex : -1,
      height : 500
  },
  arrowBack :{
      color : colors.orangeTertiary,
      position : 'absolute',
      padding : 15,
      top : 20,
      zIndex : 1
  },
  inscriptionStepper : {
      marginTop : 50,
  },
  buttonProfil : {
      marginTop : 50,
      padding : 30,
      backgroundColor : colors.blueSecondary,
      borderRadius : 30,
      position : 'relative',

  }, 
  buttonProfilText : {
      color : "#FFF",
      fontSize : 30,
      fontWeight : 'bold',
      textAlign : 'right',
      paddingRight : 10
  },
  buttonProfilImage : {
      position : 'absolute',
      width : 200,
      height :200,
      top : -70,
      left : -50
  },
  container :{
    alignItems: 'center',
    width:'100%'
  },
  imageContainer:{
    position : 'relative',
    height : '45%',
    maxHeight : '50%',
    width:'100%',
    backgroundColor : 'black',
  },
  imageHomeInscription:{
    width : '50%',
    height : '50%'
  },
  image: {
    width : '100%',
    height: '100%',
    position : 'absolute',
    opacity : 0.7
  },
  titleContainer : {
    position : 'absolute',
    zIndex : 1,
    top: '40%',
    left: '20%',
  },
  homeTitle : {
    color: '#fff',
    fontSize : 80,
    fontWeight : 'bold',
  },
  titleSpan : {
    color: '#2ECC71',
    fontSize : 80,
    fontWeight : 'bold',
  },
  subtitle : {
    color: '#fff',
    fontSize : 30,
    fontWeight : 'bold',
    width : '80%',
    textAlign : 'center'
  },
  logoBackground : {
    width : 50,
    height : 50
  },
  textContainer : {
    width:'80%',
    height: '60%',
    zIndex : 2,
    borderRadius : 20,
    borderRadius : 10,
    marginTop:30
  }, 
  buttonTitle : {
    fontSize : 20,
    textAlign : 'center',
    margin: 15,
    fontWeight : 'bold',
    color : colors.blueSecondary,
    opacity : 0.8,

  },
  textContent : {
    marginBottom : 30,
    fontSize : 18,
    fontWeight : '400',
    opacity : 0.8,
    color : colors.blueSecondary
  },
  });