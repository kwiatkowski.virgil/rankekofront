import { useState } from 'react';
import {View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import { styles } from './Inscription.styles';
import { Boutton } from '../../Components/Boutton/Boutton';
import iconRankeko from '../../assets/images/iconRankeko.png';
import conducteur from '../../assets/images/conducteur.png';
import photoAccueil from '../../assets/images/photo-accueil.jpg';
import passager from '../../assets/images/passager.png';
import { InscriptionStepper } from '../../Components/InscriptionStepper/InscriptionStepper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import { colors } from '../../assets/colors/colors';

export const  Inscription = ({ navigation }) =>   {

  const [step,setStep] = useState(0);
  const arrowBackSize = 20;

  return(
  <>
      {step === 0 &&
      <View style={styles.container} >
      <View style ={styles.imageContainer}>
        <View style ={styles.titleContainer}>
          <Text style={styles.homeTitle}>Rank<Text style={styles.titleSpan}>eko</Text></Text>
          <Text style={styles.subtitle}>Covoiturez et économisez</Text>
        </View>
        <Image style={styles.imageHomeInscription} source={photoAccueil} style={styles.image}/>
      </View>
      <View style={styles.textContainer}>
        <Text style ={styles.textContent}>Rankeko est une application de covoiturage favorisant les trajets entre collègues de votre domicile à votre lieu de travail. </Text>
        <Text style ={styles.textContent}>Veuillez vous inscrire pour commencer à utiliser l'application.</Text>
        <Text style={styles.buttonTitle}>Pas encore membre ?</Text>
        <Boutton
          color={colors.orangeTertiary}
          label={'Je m\'inscris'}
          functionPerso={()=>setStep(1)}/>
          <Text style={styles.buttonTitle}>J'ai déjà un compte</Text>
        <Boutton
          color={colors.blueSecondary}
          label={'Je me connecte'}
          functionPerso={()=>setStep(1)}/>
        </View> 
    </View>
      }
      {step === 1 &&
      <View style={styles.inscriptionContainer}>
      <TouchableOpacity onPress={()=>setStep(0)}><FontAwesomeIcon style={styles.arrowBack} icon={ faArrowLeft } size={arrowBackSize} /></TouchableOpacity>
      <InscriptionStepper active={1}/>
        <Text style={styles.title}>Votre adresse email ? </Text>
        <Text style={styles.inputTitle}>E-mail</Text>
        <TextInput style={styles.input} />
        <Text style={styles.text}>Chers utilisateurs, nous tenons à vous rassurer, votre adresse email ne sera en aucun cas utilisée à des fins commerciales.</Text>
        <View style={styles.button}><Boutton color={colors.orangeTertiary} label={'Continuer'} functionPerso={()=>setStep(2)}/></View>
        <Image source={iconRankeko} style={styles.imageBackground}/>
      </View>
      }
      {step === 2 &&
      <View style={styles.inscriptionContainer}>
        <TouchableOpacity onPress={()=>setStep(1)}><FontAwesomeIcon style={styles.arrowBack} icon={ faArrowLeft } size={arrowBackSize} /></TouchableOpacity>
        <TouchableOpacity onPress={()=>setStep(1)}><InscriptionStepper active={2}/></TouchableOpacity>
        <Text style={styles.title}>Comment vous appelez-vous ?</Text>
        <Text style={styles.inputTitle}>Nom</Text>
        <TextInput style={styles.input} />
        <Text style={styles.inputTitle}>Prénom</Text>
        <TextInput style={styles.input} />
        <View style={styles.button}><Boutton color={colors.orangeTertiary} label={'Continuer'} functionPerso={()=>setStep(3)}/></View>
        <Image source={iconRankeko} style={styles.imageBackground}/>
      </View>
      }
      {step === 3 &&
      <View style={styles.inscriptionContainer}>
        <TouchableOpacity onPress={()=>setStep(2)}><FontAwesomeIcon style={styles.arrowBack} icon={ faArrowLeft } size={arrowBackSize} /></TouchableOpacity>
        <TouchableOpacity onPress={()=>setStep(2)}><InscriptionStepper active={3}/></TouchableOpacity>
        <Text style={styles.title}>Choisissez un mot de passe</Text>
        <Text style={styles.inputTitle}>Mot de passe</Text>
        <TextInput style={styles.input} />
        <View style={styles.button}><Boutton color={colors.orangeTertiary} label={'Continuer'} functionPerso={()=>setStep(4)}/></View>
        <Image source={iconRankeko} style={styles.imageBackground}/>
      </View>
      }
      {step === 4 &&
      <View style={styles.inscriptionContainer}>
        <TouchableOpacity onPress={()=>setStep(3)}><FontAwesomeIcon style={styles.arrowBack} icon={ faArrowLeft } size={arrowBackSize} /></TouchableOpacity>
        <TouchableOpacity onPress={()=>setStep(3)}><InscriptionStepper active={4}/></TouchableOpacity>
        <Text style={styles.title}>Choisissez un type de profil</Text>
        <View style={styles.buttonProfil}><Image style={styles.buttonProfilImage} source={conducteur} />
          <TouchableOpacity onPress={()=>navigation.navigate('RegistrationDriver')}>
            <Text style={styles.buttonProfilText}>Conducteur</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonProfil}><Image style={styles.buttonProfilImage} source={passager} /><Text style={styles.buttonProfilText}>Passager</Text></View>
        <Image source={iconRankeko} style={styles.imageBackground}/>
      </View>
      }
    </>
)}