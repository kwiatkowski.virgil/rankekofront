import React, {useState} from "react";
import { View,Text, TouchableOpacity,TextInput } from "react-native";
import { styles } from './RegistrationDriver.styles';
import { InscriptionStepper } from '../../Components/InscriptionStepper/InscriptionStepper';
import {Picker} from '@react-native-picker/picker';
import axios from 'axios';

export const RegistrationDriver = () => {
  const [selectedLanguage, setSelectedLanguage] = useState();
  const [inputTargeted, setInputTargeted]= useState(false);
  const voitures = [
    { 
      id : 1,
      marque : 'Renault',
    },
    { 
      id : 2,
      marque : 'Kia',
    },
    { 
      id : 3,
      marque : 'Ford',
    },
  ]
console.log(selectedLanguage);

const options = {
  method: 'GET',
  url: 'https://movie-database-imdb-alternative.p.rapidapi.com/',
  params: {s: 'Avengers Endgame', r: 'json', page: '1'},
  headers: {
    'x-rapidapi-host': 'movie-database-imdb-alternative.p.rapidapi.com',
    'x-rapidapi-key': '0df6f7afa8msh9172d42d8cc89fap18012ajsnedf383bca355'
  }
};
axios.get("https://apis.solarialabs.com/shine/v1/vehicle-stats/fuel-usage?make=kia&car-truck=rio&apikey=2fRUWTyv9Ivqp3m2TA2wp5X0SfHRgYIA").then(function (response) {
  console.log(response);
})
.catch(function (error) {
  console.log(error);
});
  return(

  <View style={styles.container}>
    <TouchableOpacity><InscriptionStepper /></TouchableOpacity>
    <Text>Vous y êtes presque</Text>
    {/* <TouchableOpacity onPress={()=>setInputTargeted(true)}><Text style={styles.input}>{selectedLanguage}</Text></TouchableOpacity> */}
    {inputTargeted && <Picker
      onValueChange={(itemValue, itemIndex) =>
      setSelectedLanguage(itemValue)
      }>
      <Picker.Item label="Java" value="Java" />
      <Picker.Item label="JavaScript" value="Javascript" />
    </Picker>}
  </View>
)};