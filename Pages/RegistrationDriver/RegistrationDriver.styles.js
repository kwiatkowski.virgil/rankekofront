import { StyleSheet } from "react-native";
import { colors } from "../../assets/colors/colors";
export const styles = StyleSheet.create({
    container : {
        width : '80%',
        marginHorizontal : '10%',

    },
    input : {
        marginTop : 10,
        marginBottom : 30,
        height : 40,
        width : '100%',
        textAlign: 'center',
        justifyContent : 'center',
        backgroundColor: "#fff",
        shadowColor: colors.blueSecondary,
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        shadowRadius: 4,
        borderRadius : 10,

    },
 });