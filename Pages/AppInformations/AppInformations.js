import React from "react";
import { View } from "react-native";
import { Driver } from '../Driver/Driver';
import { Passenger } from '../Passenger/Passenger';
import { styles } from './AppInformations.styles';


export const AppInformations = () => (
  <View style={styles.container}>
      {/* <Driver /> */}
      <Passenger />
  </View>
);