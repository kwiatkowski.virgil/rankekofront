export const colors = {
    greenPrimary: '#2ECC71',
    blueSecondary : '#2C3E50',
    orangeTertiary: '#EF7C60',
}